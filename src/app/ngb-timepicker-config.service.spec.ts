import { TestBed } from '@angular/core/testing';

import { NgbTimepickerConfigService } from './ngb-timepicker-config.service';

describe('NgbTimepickerConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgbTimepickerConfigService = TestBed.get(NgbTimepickerConfigService);
    expect(service).toBeTruthy();
  });
});
