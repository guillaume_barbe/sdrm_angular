import { Component, ViewChild, ElementRef, HostListener } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { ToggleService } from './toggle.service';

@Component({
   selector: 'app-root',
   templateUrl: './app.component.html',
   styleUrls: ['./app.component.css']
})
export class AppComponent {

   sticky: boolean = false;
   
   menuPosition: any;

   constructor() { }

   ngOnInit() {
   }
   
   @ViewChild('stickyMenu, stickysidenav',{static:false}) menuElement: ElementRef;
 
   @HostListener('window:scroll', ['$event'])
   handleScroll(){
      const windowScroll = window.pageYOffset;
        if(windowScroll >= this.menuPosition){
            this.sticky = true;
        } else {
            this.sticky = false;
        }
   }

   ngAfterViewInit(){
      this.menuPosition = this.menuElement.nativeElement.offsetTop; 
  }


}