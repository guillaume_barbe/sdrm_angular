import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MaterialModule} from '../material-module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { SiteHeaderComponent } from './layout/site-header/site-header.component';
import { SiteFooterComponent } from './layout/site-footer/site-footer.component';
import { SidenavComponent } from './layout/sidenav/sidenav.component';
import { AccueilComponent } from './accueil/accueil.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ToggleService } from './toggle.service';
import {CountriesService} from './countries.service';
// import {NgbTimepickerConfigService}  from './ngb-timepicker-config.service';

import { HeaderfixedComponent } from './layout/headerfixed/headerfixed.component';
import { StepperrequestComponent } from './demande-autorisation/stepperrequest/stepperrequest.component';

import { DemandeAutorisationComponent } from './demande-autorisation/demande-autorisation.component';
import { Step1InformationsSupportComponent } from './demande-autorisation/step1-informations-support/step1-informations-support.component';
import { Step2QuantitePrixComponent } from './demande-autorisation/step2-quantite-prix/step2-quantite-prix.component';
import { Step3IntervenantsComponent } from './demande-autorisation/step3-intervenants/step3-intervenants.component';
import { Step4CasParticuliersComponent } from './demande-autorisation/step4-cas-particuliers/step4-cas-particuliers.component';
import { Step5ListeOeuvresComponent } from './demande-autorisation/step5-liste-oeuvres/step5-liste-oeuvres.component';

import { HttpClientModule } from '@angular/common/http';
import { FabricantComponent } from './demande-autorisation/step3-intervenants/fabricant/fabricant.component';
import { DonneurOrdreComponent } from './demande-autorisation/step3-intervenants/donneur-ordre/donneur-ordre.component';
import { DistributeurComponent } from './demande-autorisation/step3-intervenants/distributeur/distributeur.component';
import { DialogAjouterOeuvreComponent } from './demande-autorisation/step5-liste-oeuvres/dialog-ajouter-oeuvre/dialog-ajouter-oeuvre.component';
import { AyantDroitTableComponent } from './demande-autorisation/step5-liste-oeuvres/dialog-ajouter-oeuvre/ayant-droit-table/ayant-droit-table.component';
import { DialogAjouterAyantDroitComponent } from './demande-autorisation/step5-liste-oeuvres/dialog-ajouter-oeuvre/ayant-droit-table/dialog-ajouter-ayant-droit/dialog-ajouter-ayant-droit.component';

const appRoutes: Routes = [
  { path: 'demande', component: DemandeAutorisationComponent },
  { path: '', component: AccueilComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    DemandeAutorisationComponent,
    SiteHeaderComponent,
    SiteFooterComponent,
    SidenavComponent,
    AccueilComponent,
    NavbarComponent,
    HeaderfixedComponent,
    StepperrequestComponent,
    Step1InformationsSupportComponent,
    Step2QuantitePrixComponent,
    Step3IntervenantsComponent,
    Step4CasParticuliersComponent,
    Step5ListeOeuvresComponent,
    FabricantComponent,
    DonneurOrdreComponent,
    DistributeurComponent,
    DialogAjouterOeuvreComponent,
    AyantDroitTableComponent,
    DialogAjouterAyantDroitComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MaterialModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    NgbModule
  ],
  entryComponents: [
    DialogAjouterOeuvreComponent,
    DialogAjouterAyantDroitComponent
  ],
  providers: [ToggleService, CountriesService],
  bootstrap: [AppComponent]
})
export class AppModule { }

