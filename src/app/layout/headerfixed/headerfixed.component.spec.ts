import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderfixedComponent } from './headerfixed.component';

describe('HeaderfixedComponent', () => {
  let component: HeaderfixedComponent;
  let fixture: ComponentFixture<HeaderfixedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderfixedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderfixedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
