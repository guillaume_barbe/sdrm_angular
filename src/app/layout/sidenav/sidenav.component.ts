import { Component, OnInit, ViewChild } from '@angular/core';
import { ToggleService } from '../../toggle.service';
import { MatSidenav } from '@angular/material';


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})


export class SidenavComponent implements OnInit {
  
  @ViewChild('sidenav', {static: false})  sidenav: MatSidenav;

  constructor(public toggleService:ToggleService) {
  }

  ngOnInit(): void {
    this.toggleService.setSidenav(this.sidenav);
  }
  

}
