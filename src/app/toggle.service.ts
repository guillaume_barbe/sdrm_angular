import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ToggleService {
  private sidenav: MatSidenav;

  constructor() { }

  setSidenav(sidenav: MatSidenav) {
      this.sidenav = sidenav;
  }

 open() {
      return this.sidenav.open();
  }

 close() {
      return this.sidenav.close();
  }

  toggle(isOpen?: boolean) {
  this.sidenav.toggle(isOpen);
 }

  
}
