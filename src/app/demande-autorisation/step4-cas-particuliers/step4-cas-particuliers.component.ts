import { Component, OnInit, Input  } from '@angular/core';
import { FormGroup} from '@angular/forms';

@Component({
  selector: 'step4-cas-particuliers',
  templateUrl: './step4-cas-particuliers.component.html',
  styleUrls: ['./step4-cas-particuliers.component.css']
})
export class Step4CasParticuliersComponent implements OnInit {

  @Input() regForm: FormGroup;
  showref: string;
  constructor() { }


  ngOnInit() {
    
  }

  json() {
    let data = this.regForm.value;
    console.log(data);
  }
  
}
