import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Step4CasParticuliersComponent } from './step4-cas-particuliers.component';

describe('Step4CasParticuliersComponent', () => {
  let component: Step4CasParticuliersComponent;
  let fixture: ComponentFixture<Step4CasParticuliersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Step4CasParticuliersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Step4CasParticuliersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
