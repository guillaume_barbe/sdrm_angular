import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';


@Component({
  selector: 'app-demande-autorisation',
  templateUrl: './demande-autorisation.component.html',
  styleUrls: ['./demande-autorisation.component.css']
})
export class DemandeAutorisationComponent implements OnInit {

  registrationForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.registrationForm = this.fb.group({
      'supportFG': this.fb.group({
        'titre': ['', Validators.required],
        'interprete': ['', Validators.required],
        'ref': ['', Validators.compose([Validators.required, Validators.maxLength(5)])],
        'code_EAN': [''],
        'type_support': ['', Validators.required],
        'numero_support': ['', Validators.required]
      }),
      'quantitePrixFG': this.fb.group({
        'quantite_fabriquer': ['', Validators.required],
        'export': [''],
        'destination_vente': ['', Validators.required],
        'quantite_vente': [],
        'prix_unitaire': [],
        'destination_gratuit': [],
        'quantite_gratuit': []
      }),
      'intervenantFG': this.fb.group({
        'fabricantFG': this.fb.group({
          'raison_sociale': ['', Validators.required],
          'num_voie': ['', Validators.required],
          'type_voie': ['', Validators.required],
          'nom_voie': ['', Validators.required],
          'code_postal': ['', Validators.required],
          'ville': ['', Validators.required],
          'pays': ['', Validators.required],
          'telephone': ['', [Validators.required, Validators.maxLength(10), Validators.pattern("^[0-9]*$")]],
          'fax': ['', Validators.required],
          'email': ['', Validators.required]
        }),
        'donneurFG': this.fb.group({
          'raison_sociale': ['', Validators.required],
          'num_voie': ['', Validators.required],
          'type_voie': ['', Validators.required],
          'nom_voie': ['', Validators.required],
          'code_postal': ['', Validators.required],
          'ville': ['', Validators.required],
          'pays': ['', Validators.required],
          'telephone': ['', Validators.required],
          'fax': ['', Validators.required],
          'email': ['', Validators.required]
        }),
        'distributeurFG': this.fb.group({
          'raison_sociale': ['', Validators.required],
          'num_voie': ['', Validators.required],
          'type_voie': ['', Validators.required],
          'nom_voie': ['', Validators.required],
          'code_postal': ['', Validators.required],
          'ville': ['', Validators.required],
          'pays': ['', Validators.required],
          'telephone': ['', Validators.required],
          'fax': ['', Validators.required],
          'email': ['', Validators.required]
        }),
      }),
      'casparticuliersFG': this.fb.group({
        'ayant-droit': ['', Validators.required],
        'auto-prod': ['', Validators.required],
        'compilation': ['', Validators.required],
        'rep-enregistrement': ['', Validators.required],
        'reference-enregistrement-repris': ['', Validators.required]
      }),
      'listeoeuvresFG': this.fb.group({
        'oeuvres': this.fb.array([]),
      })
    });
  }



  // dialog box add fields
  // patch() {
  //   const control = <FormArray>this.registrationForm.get('listeoeuvresFG.oeuvres');
  //   this.registrationForm.get('listeoeuvresFG').oeuvres.forEach(element => {

  //   });
  //     control.push(this.patchValues(x.label, x.value))
  //   })
  // }

  // patchValues(titre_oeuvre) {
  //   return this.fb.group({
  //     titre_oeuvre: ['', Validators.required]
  //   })
  // }


  get f() {
    return this.registrationForm['controls']
    console.log(this.registrationForm.controls)
  }

  submitForm() {
    let credentials = this.registrationForm.value;
    console.log(credentials)
    console.log(this.registrationForm.controls);
  }





}

