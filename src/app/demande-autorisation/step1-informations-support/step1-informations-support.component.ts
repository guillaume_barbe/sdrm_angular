import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

export interface Support {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'step1-informations-support',
  templateUrl: './step1-informations-support.component.html',
  styleUrls: ['./step1-informations-support.component.css']
})
export class Step1InformationsSupportComponent implements OnInit {

  supports: Support[] = [
    { value: 'cd', viewValue: 'CD' },
    { value: 'cassette', viewValue: 'Cassette' },
    { value: 'vinyle', viewValue: 'Vinyle' },
  ];

  @Input() regForm: FormGroup;

  constructor() { }

  ngOnInit() {

  }

  json() {

    let data = this.regForm.value;
    console.log(data);
  }

  // ******Autosize mat-form-field******
   @ViewChild('formFieldInput', { static: false }) formFieldInputER: ElementRef;
   @ViewChild('invisibleText', { static: false }) invTextER: ElementRef;
   @ViewChild('invisibleText1', { static: false }) invTextER1: ElementRef;
   @ViewChild('inputText', { static: false }) inputTextER: ElementRef;
   @ViewChild('inputText1', { static: false }) inputTextER1: ElementRef;

  inString: string = "";
  width: number = 10;
  width1: number = 10;
  inputExample = '';
  inputExample1 = '';

  resizeInput(inputText) {
    // without setTimeout the width gets updated to the previous length
    setTimeout ( () =>{
      this.width = this.invTextER.nativeElement.offsetWidth + 20;
      this.width1 = this.invTextER1.nativeElement.offsetWidth + 20;
    }, 0)
  }


}

// https://github.com/srikanthmadasu/angular-material-stepper-example/blob/master/src/app/components/registration-step1/registration-step1.component.ts
// step1Submitted() {
//   this.regForm.get('personalDetails').get('firstname').markAsTouched();
//   this.regForm.get('personalDetails').get('firstname').updateValueAndValidity();
//   this.regForm.get('personalDetails').get('lastname').markAsTouched();
//   this.regForm.get('personalDetails').get('lastname').updateValueAndValidity();
// }

