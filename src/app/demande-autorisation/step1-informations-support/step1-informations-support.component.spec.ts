import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationsSupportComponent } from './informations-support.component';

describe('InformationsSupportComponent', () => {
  let component: InformationsSupportComponent;
  let fixture: ComponentFixture<InformationsSupportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationsSupportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationsSupportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
