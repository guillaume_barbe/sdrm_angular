import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperrequestComponent } from './stepperrequest.component';

describe('StepperrequestComponent', () => {
  let component: StepperrequestComponent;
  let fixture: ComponentFixture<StepperrequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepperrequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperrequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
