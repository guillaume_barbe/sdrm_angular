import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Step5ListeOeuvresComponent } from './step5-liste-oeuvres.component';

describe('Step5ListeOeuvresComponent', () => {
  let component: Step5ListeOeuvresComponent;
  let fixture: ComponentFixture<Step5ListeOeuvresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Step5ListeOeuvresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Step5ListeOeuvresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
