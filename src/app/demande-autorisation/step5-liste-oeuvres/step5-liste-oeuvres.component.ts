
import { Component, ViewChild, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, AbstractControl, Validators } from '@angular/forms';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog, MatTable } from '@angular/material';
import { MatTableDataSource } from '@angular/material';


export interface Type_Oeuvre {
  value: string;
  viewValue: string;
}

export interface Ayant_droit {
  value: string;
  viewValue: string;
}


export interface Ayants {
  id: number,
  nom: string,
  prenom: string,
  profession: string,
}
export interface Duree {
  hour: number,
  minute: number,
  second: number
}

export interface OeuvresData {
  id: number;
  titre_oeuvre: string;
  type_oeuvre: string;
  duree: Duree;
  fragement: boolean;
  ayants: Ayants[];
}

const ELEMENT_DATA: OeuvresData[] = [
  { id: 1, titre_oeuvre: 'Hélène', type_oeuvre: 'Medley', fragement: false, duree: { hour: 1, minute: 10, second: 36 }, ayants: [{ id: 1, profession: 'compositeur', nom: 'Arthur', prenom: 'Altin' }] },
  { id: 2, titre_oeuvre: 'Francky', type_oeuvre: 'chanson', fragement: false, duree: { hour: 0, minute: 3, second: 36 }, ayants: [{ id: 2, profession: 'editeur', nom: 'Jules', prenom: 'Altin' }] },
];

@Component({
  selector: 'step5-liste-oeuvres',
  templateUrl: './step5-liste-oeuvres.component.html',
  styleUrls: ['./step5-liste-oeuvres.component.css']
})
export class Step5ListeOeuvresComponent implements OnInit {

  dataSource = ELEMENT_DATA;
  @Input() regForm: FormGroup;

  openformcreate: boolean
  closeform: boolean
 


  time: NgbTimeStruct = { hour: null, minute: null, second: null };

  // dataSourceTable = new MatTableDataSource<Oeuvres>();
  // oeuvreData: Oeuvres[] = [];
  // @ViewChild(MatTable, { static: true }) table: MatTable<any>;
  // displayedColumns: string[] = ['id', 'oeuvre'];
  // dataSourceTable= ELEMENT_DATA;

  constructor(private fb: FormBuilder, config: NgbTimepickerConfig) {
    this.regForm = this.fb.group({
      listeoeuvresFG: this.fb.group({
        oeuvres: this.fb.array([])
      })
    });

    config.seconds = true;
    config.spinners = false;
  }




  ngOnInit() {
    this.initForm();
    console.log(this.regForm.value)
  }

  initForm() {
    const control = <FormArray>this.regForm.get('listeoeuvresFG').get('oeuvres');
    console.log(control)
    this.dataSource.forEach((x) => {
      control.push(this.fb.group({
        id: [x.id],
        titre_oeuvre: [x.titre_oeuvre],
        type_oeuvre: [x.type_oeuvre],
        fragement: [x.fragement],
        duree: [x.duree],
        ayants: this.setAyant(x)
      }))
    })

  }


  setAyant(x) {
    let arr = new FormArray([])
    x.ayants.forEach(y => {
      arr.push(this.fb.group({
        id: [y.id],
        nom: [y.nom],
        prenom: [y.prenom],
        profession: [y.profession]
      }))
    })
    return arr;
  }

  addOeuvre() {
    var d = new Date();
    this.openformcreate = true;
    let control = (<FormArray>this.regForm.controls['listeoeuvresFG']).controls['oeuvres'];
    control.push(
      this.fb.group({
        id: d.getTime(),
        titre_oeuvre: ['', Validators.required],
        type_oeuvre: [''],
        fragement: [''],
        duree: [''],
        ayants: this.fb.array([])
      })
    )
  }

  deleteOeuvre(index) {
    let control = (<FormArray>this.regForm.controls['listeoeuvresFG']).controls['oeuvres'];
    control.removeAt(index)
  }

  addAyant(control) {
    var d = new Date();
    control.push(
      this.fb.group({
        id: d.getTime(),
        nom: [''],
        prenom: [''],
        profession: ['']
      }))
  }

  deleteAyant(control, index) {
    control.removeAt(index)
  }

  onSubmit() {
  
    console.log(this.regForm.value);
    console.log('Form JSON',JSON.stringify(this.regForm.value));
  }

  submitOeuvre(){
    console.log('houra');
    this.openformcreate = false;
  }

  type_oeuvres: Type_Oeuvre[] = [
    { value: 'chanson', viewValue: 'chanson' },
    { value: 'musique instrumentale', viewValue: 'musique instrumentale' },
    { value: 'medley', viewValue: 'Medley' },
    { value: 'texte parlé', viewValue: 'Texte parlé' },
    { value: 'conte musical', viewValue: 'conte musical' },
    { value: 'literaire', viewValue: 'literaire' },
    { value: 'spectacle', viewValue: 'spectacle' },
    { value: 'poeme', viewValue: 'poeme' },
    { value: 'discours', viewValue: 'discours' },
    { value: 'entretien', viewValue: 'entretien' },
    { value: 'méthode', viewValue: 'méthode' },
  ];

  ayant_droits: Ayant_droit[] = [
    { value: 'auteur', viewValue: 'auteur' },
    { value: 'compositeur', viewValue: 'compositeur' },
    { value: 'interprète', viewValue: 'interprète' },
    { value: 'arrangeur', viewValue: 'arrangeur' },
    { value: 'editeur', viewValue: 'editeur' },
    { value: 'adptateur', viewValue: 'adptateur' }
  ];


}



  // ------ json model --------------

  // data = {
  //   oeuvres: [
  //     {
  //       oeuvre: "",
  //       ayants: [
  //         {
  //           nom: "",
  //         }
  //       ]
  //     }
  //   ]
  // }


  // setOeuvres() {
  //   let control = <FormArray>this.regForm.controls.oeuvres;
  //   this.data.oeuvres.forEach(x => {
  //     control.push(this.fb.group({
  //       oeuvre: x.oeuvre,
  //       ayants: this.setAyant(x)
  //     }))
  //   })
  // }

  // ----------------------------------





// import { Component, OnInit, Input, ViewChild } from '@angular/core';
// import { FormGroup, FormArray, Validators, FormControl, FormBuilder } from '@angular/forms';
// import { MatDialog, MatTable } from '@angular/material';
// import { DialogAjouterOeuvreComponent } from './dialog-ajouter-oeuvre/dialog-ajouter-oeuvre.component';


// export interface Oeuvresdonnees {
//   id: number;
//   titre_oeuvre: string;
//   type_oeuvre: string;
//   duree: object;
//   fragement: boolean;
//   ayants: object;
// }

// const ELEMENT_DATA: Oeuvresdonnees[] = [
//   { id: 1, titre_oeuvre: 'Hélène', type_oeuvre: 'Medley', fragement: false, duree: { hour: 1, minute: 10, second: 36 }, ayants: [{ profession: 'd', nom: 'a' }, { profession: 'c', nom: 'l' }] },
//   { id: 2, titre_oeuvre: 'Hélène', type_oeuvre: 'Medley', fragement: false, duree: { hour: 1, minute: 10, second: 36 }, ayants: [{ profession: 'd', nom: 'a' }, { profession: 'c', nom: 'l' }] },

// ];


// @Component({
//   selector: 'step5-liste-oeuvres',
//   templateUrl: './step5-liste-oeuvres.component.html',
//   styleUrls: ['./step5-liste-oeuvres.component.css']
// })
// export class Step5ListeOeuvresComponent implements OnInit {
//   displayedColumns: string[] = ['titre_oeuvre', 'type_oeuvre', 'duree', 'fragement', 'ayants', 'action'];

//   dataSource = ELEMENT_DATA;

//   @Input() regForm: FormGroup;

//   @ViewChild(MatTable, { static: true }) table: MatTable<any>;

//   constructor(public dialog: MatDialog, private fb: FormBuilder) {

//   }



//   json() {
//     let data = this.regForm.value;
//     console.log('data form', data);
//   }


//   openDialog(action, obj) {
//     obj.action = action;
//     const dialogRef = this.dialog.open(DialogAjouterOeuvreComponent, {
//       width: '700px',
//       data: obj
//     });

//     dialogRef.afterClosed().subscribe(result => {
//       if (result.event == 'Add') {
//         this.addRowData(result.data);
//       } else if (result.event == 'Update') {
//         this.updateRowData(result.data);
//       } else if (result.event == 'Delete') {
//         this.deleteRowData(result.data);
//       }
//     });
//   }


//   addRowData(o) {
//     var d = new Date();
//     var f =  [{nom:"aa", profession: "aa"},{nom:"bb", profession: "bb"} ]
//     this.dataSource.push({
//       id: d.getTime(),
//       titre_oeuvre: o.titre_oeuvre,
//       type_oeuvre: o.type_oeuvre,
//       duree: o.duree,
//       fragement: o.fragement,
//       ayants : f


//     });
//     this.addOeuvresForm();
//     this.table.renderRows();
//   }

//   updateRowData(o) {
//     this.dataSource = this.dataSource.filter((value, key) => {
//       if (value.id == o.id) {
//         value.titre_oeuvre = o.titre_oeuvre;
//         value.type_oeuvre = o.type_oeuvre;
//         value.duree = o.duree;
//         value.fragement = o.fragement;
//         value.ayants = o.ayants;

//       }
//       return true;
//     });
//   }
//   deleteRowData(o) {
//     this.dataSource = this.dataSource.filter((value, key) => {
//       return value.id != o.id;
//     });
//   }


//   ngOnInit() {

//     this.addOeuvresForm();
//     console.log(this.addOeuvresForm())
//     this.regForm.get('listeoeuvresFG').valueChanges.subscribe(oeuvres => { console.log( oeuvres) });
//   }

//   addOeuvresForm() {
//     const control = <FormArray>this.regForm.get('listeoeuvresFG').get('oeuvres');

//     this.dataSource.forEach((x) => {
//       control.push(this.fb.group({
//         id: [x.id],
//         titre_oeuvre: [x.titre_oeuvre],
//         type_oeuvre: [x.type_oeuvre],
//         duree: [x.duree],
//         fragement: [x.fragement],
//         ayants: this.setAyants(x)
//       }))
//     })
//   };


//   setAyants(x) {
//     let arr = new FormArray([])
//     x.ayants.forEach(y => {
//       arr.push(this.fb.group({
//         profession: [y.profession],
//         nom: [y.nom]
//       }))
//     })
//     return arr;
//   }



// }








// WITH TABLE 

// @Component({
//   selector: 'step5-liste-oeuvres',
//   templateUrl: './step5-liste-oeuvres.component.html',
//   styleUrls: ['./step5-liste-oeuvres.component.css']
// })
// export class Step5ListeOeuvresComponent implements OnInit {
//   displayedColumns: string[] = ['titre_oeuvre', 'type_oeuvre', 'duree', 'fragement', 'ayants', 'action'];

//   dataSource = ELEMENT_DATA;


//   @Input() regForm: FormGroup;

//   @ViewChild(MatTable, { static: true }) table: MatTable<any>;

//   constructor(public dialog: MatDialog, private fb: FormBuilder) {
//   }


//   ngOnInit() {

//     this.addOeuvresForm();
//     console.log(this.addOeuvresForm())

//   }


//   json() {
//     this.regForm.value;
//     console.log('data form', this.regForm.get('listeoeuvresFG').get('oeuvres').value);

//   }


//   openDialog(action, obj) {
//     obj.action = action;
//     const dialogRef = this.dialog.open(DialogAjouterOeuvreComponent, {
//       width: '700px',
//       data: obj,

//     });

//     dialogRef.afterClosed().subscribe(result => {
//       if (result.event == 'Ajouter') {
//         this.addRowData(result.data);
//         console.log(result.data);
//       } else if (result.event == 'Modifier') {
//         this.updateRowData(result.data);
//       } else if (result.event == 'Supprimer') {
//         this.deleteRowData(result.data);
//       }
//     });
//   }


//   addRowData(o) {
//     var d = new Date();
//     this.dataSource.push({
//       id: d.getTime(),
//       titre_oeuvre: o.titre_oeuvre,
//       type_oeuvre: o.type_oeuvre,
//       duree: o.duree,
//       fragement: o.fragement,
//       ayants : o.ayants
//     });
//     this.addOeuvresForm();
//     this.table.renderRows();
//   }

//   updateRowData(o) {
//     this.dataSource = this.dataSource.filter((value, key) => {
//       if (value.id == o.id) {
//         value.titre_oeuvre = o.titre_oeuvre;
//         value.type_oeuvre = o.type_oeuvre;
//         value.duree = o.duree;
//         value.fragement = o.fragement;
//         value.ayants = o.ayants;
//         console.log(o.ayants)
//         console.log(o.fragement)

//       }
//       return true;
//     });
//     this.addOeuvresForm();
//   }
//   deleteRowData(o) {
//     this.dataSource = this.dataSource.filter((value, key) => {

//       return value.id != o.id; 
//     });
//   }



//   addOeuvresForm() {
//     const controlAyants = <FormArray>this.regForm.get('listeoeuvresFG').get('oeuvres').get('ayants');
//     console.log(controlAyants )
//     const control = <FormArray>this.regForm.get('listeoeuvresFG').get('oeuvres');
//     control.controls= [];
//     this.dataSource.forEach((x) => {
//       control.push(this.fb.group({
//         id: [x.id],
//         titre_oeuvre: [x.titre_oeuvre],
//         type_oeuvre: [x.type_oeuvre],
//         duree: [x.duree],
//         fragement: [x.fragement],
//         ayants: this.setAyants(x)


//       }))
//     })
//     console.log(control);
//   };


//   setAyants(x) {
//     let arr = new FormArray([])
//     console.log(x)
//     console.log(x.ayants)
//     x.ayants.forEach(y => {
//       arr.push(this.fb.group({
//         id: [y.id],
//         profession: [y.profession],
//         nom: [y.nom],
//         prenom: [y.prenom]
//       }))
//     })
//     return arr;
//   }







// }



