import { Component, Inject, Optional, OnInit,Injectable, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormArray, AbstractControl } from '@angular/forms';



export interface Oeuvre {
  value: string;
  viewValue: string;
}

export interface Ayant_droit {
  value: string;
  viewValue: string;
}


export interface Ayants {
  id: number,
  nom: string,
  prenom: string,
  profession: string,
}
export interface Duree {
  hour: number,
  minute: number,
  second: number
}


export interface Oeuvresdonnees {
  id: number;
  titre_oeuvre: string;
  type_oeuvre: string;
  duree: Duree;
  fragement: boolean;
  ayants: Ayants[];
}


@Component({
  selector: 'app-dialog-ajouter-oeuvre',
  templateUrl: './dialog-ajouter-oeuvre.component.html',
  styleUrls: ['./dialog-ajouter-oeuvre.component.css']
})
export class DialogAjouterOeuvreComponent implements OnInit{

  time: NgbTimeStruct = { hour: null, minute: null, second: null };

  action: string;
  local_data: any;
  regForm: FormGroup;
  


  constructor(
    public dialogRef: MatDialogRef<DialogAjouterOeuvreComponent>,
    //@Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Oeuvresdonnees,
    config: NgbTimepickerConfig, private fb: FormBuilder) {

  
    // config.minuteStep = 10;
    config.seconds = true;
    config.spinners = false;

    this.local_data = { ...data };
    
    this.action = this.local_data.action;
  }


  ngOnInit() {
    
    this.regForm;
    console.log(this.regForm)
  }


  doAction() {
    this.dialogRef.close({ event: this.action, data: this.local_data});
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }

  type_oeuvres: Oeuvre[] = [
    { value: 'chanson', viewValue: 'chanson' },
    { value: 'musique instrumentale', viewValue: 'musique instrumentale' },
    { value: 'medley', viewValue: 'Medley' },
    { value: 'texte parlé', viewValue: 'Texte parlé' },
    { value: 'conte musical', viewValue: 'conte musical' },
    { value: 'literaire', viewValue: 'literaire' },
    { value: 'spectacle', viewValue: 'spectacle' },
    { value: 'poeme', viewValue: 'poeme' },
    { value: 'discours', viewValue: 'discours' },
    { value: 'entretien', viewValue: 'entretien' },
    { value: 'méthode', viewValue: 'méthode' },
  ];

  ayant_droits: Ayant_droit[] = [
    { value: 'auteur', viewValue: 'auteur' },
    { value: 'compositeur', viewValue: 'compositeur' },
    { value: 'interprète', viewValue: 'interprète' },
    { value: 'arrangeur', viewValue: 'arrangeur' },
    { value: 'editeur', viewValue: 'editeur' },
    { value: 'adptateur', viewValue: 'adptateur' }
  ];


}


