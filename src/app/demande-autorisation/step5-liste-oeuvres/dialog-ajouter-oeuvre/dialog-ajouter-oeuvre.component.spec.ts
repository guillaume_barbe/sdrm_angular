import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAjouterOeuvreComponent } from './dialog-ajouter-oeuvre.component';

describe('DialogAjouterOeuvreComponent', () => {
  let component: DialogAjouterOeuvreComponent;
  let fixture: ComponentFixture<DialogAjouterOeuvreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAjouterOeuvreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAjouterOeuvreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
