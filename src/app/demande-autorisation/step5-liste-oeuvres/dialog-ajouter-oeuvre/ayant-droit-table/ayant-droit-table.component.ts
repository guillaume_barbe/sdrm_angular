import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, NgForm, FormArrayName, FormGroupName } from '@angular/forms'
import { MatDialog, MatTable } from '@angular/material';
import { DialogAjouterAyantDroitComponent } from './dialog-ajouter-ayant-droit/dialog-ajouter-ayant-droit.component'

export interface ayant_droit {
  value: string;
  viewValue: string;
}


export interface personne_ayant_droits {
  id: number,
  profession: string,
  nom: string;
  prenom: string;
}


export interface Ayants {
  id: number,
  nom: string,
  prenom: string,
  profession: string,
}


const ELEMENT_DATA: personne_ayant_droits[] = [
  { id: 1, profession: 'compositeur', nom: 'John', prenom: 'jules' },
  { id: 2, profession: 'arrangeur', nom: 'Joe', prenom: 'jasmin' }
];



@Component({
  selector: 'app-ayant-droit-table',
  templateUrl: './ayant-droit-table.component.html',
  styleUrls: ['./ayant-droit-table.component.css']
})
export class AyantDroitTableComponent implements OnInit {

  @Input() data: any[]

  @Input() regForm: FormGroup;



  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  displayedColumns: string[] = ['nom', 'prénom', 'profession', 'action'];
  dataSource: any[]=[];



  constructor(public dialog: MatDialog, private fb: FormBuilder) {

    this.regForm = this.fb.group({
      ayants: this.fb.array([]),
    });

    // this.dataSource.data = this.personnes_ayant_droits;
    // this.dataSource.filterPredicate = this.createFilter();

  }




  ngOnInit() {
    if (typeof this.data === 'undefined')
      this.dataSource  =[]
    else
    this.dataSource = this.data;



    // this.nameFilter.valueChanges
    //   .subscribe(
    //     name => {
    //       this.filterValues.name = name;
    //       this.dataSource.filter = JSON.stringify(this.filterValues);
    //     }
    //   )
  }

  // nameFilter = new FormControl('');
  // filterValues = {
  //   name: ''
  // };

  ayant_droits: ayant_droit[] = [
    { value: 'auteur', viewValue: 'auteur' },
    { value: 'compositeur', viewValue: 'compositeur' },
    { value: 'interprète', viewValue: 'interprète' },
    { value: 'arrangeur', viewValue: 'arrangeur' },
    { value: 'editeur', viewValue: 'editeur' },
    { value: 'adptateur', viewValue: 'adptateur' }
  ];

  openDialog(action, obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogAjouterAyantDroitComponent, {
      width: '400px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Ajouter') {
        this.addRowData(result.data);
      } else if (result.event == 'Modifier') {
        this.updateRowData(result.data);
      } else if (result.event == 'Supprimer') {
        this.deleteRowData(result.data);
      }
    });
  }

  addRowData(row_obj) {
    var d = new Date();
    // console.log(this.dataSource)
    this.dataSource.push({
      id: d.getTime(),
      profession: row_obj.profession,
      nom: row_obj.nom,
      prenom: row_obj.prenom,
    });
    this.table.renderRows();
  }

  updateRowData(row_obj) {
    this.dataSource = this.dataSource.filter((value, key) => {
      if (value.id == row_obj.id) {
        value.profession = row_obj.profession;
        value.nom = row_obj.nom;
        value.prenom = row_obj.prenom;

      }
      return true;
    });
  }
  deleteRowData(row_obj) {
    this.dataSource = this.dataSource.filter((value, key) => {

      return value.id != row_obj.id;
    });

  }

   setAyantForm() {
    const Ctrl = this.regForm.controls.ayants as FormArray;
    console.log(Ctrl);
    Ctrl.controls = [];
    this.dataSource.forEach((ayant) => {
      Ctrl.push(this.setAyantFormArray(ayant))
    })
    console.log(this.dataSource);
    console.log(Ctrl.value);
  };

  private setAyantFormArray(o) {
    return this.fb.group({
      id: [o.id],
      profession: [o.profession],
      nom: [o.nom],
      prenom: [o.prenom],
    });
  }



}




  // ---------------------//

  // createFilter(): (data: any, filter: string) => boolean {
  //   let filterFunction = function (data, filter): boolean {
  //     let searchTerms = JSON.parse(filter);
  //     return data.name.toLowerCase().indexOf(searchTerms.name) !== -1;
  //   }
  //   return filterFunction;
  // }

