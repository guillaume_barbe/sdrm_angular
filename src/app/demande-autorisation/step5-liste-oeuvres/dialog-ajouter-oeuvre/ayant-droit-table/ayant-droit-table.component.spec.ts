import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AyantDroitTableComponent } from './ayant-droit-table.component';

describe('AyantDroitTableComponent', () => {
  let component: AyantDroitTableComponent;
  let fixture: ComponentFixture<AyantDroitTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AyantDroitTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AyantDroitTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
