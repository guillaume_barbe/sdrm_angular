import { Component, Inject, Optional, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, NgForm } from '@angular/forms'

export interface Ayant_droit {
  value: string;
  viewValue: string;
}


export interface Ayant {
  id: number;
  profession: string;
  nom: string;
  prenom: string;
}

@Component({
  selector: 'app-dialog-ajouter-ayant-droit',
  templateUrl: './dialog-ajouter-ayant-droit.component.html',
  styleUrls: ['./dialog-ajouter-ayant-droit.component.css']
})
export class DialogAjouterAyantDroitComponent {

  action: string;
  local_data: any;
  @Input() regForm: FormGroup;


  constructor(
    public dialogRef: MatDialogRef<DialogAjouterAyantDroitComponent>,
    //@Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Ayant,
  ) {
    this.local_data = { ...data };
    // console.log(data);
    this.action = this.local_data.action;
  }

  doAction() {
    this.dialogRef.close({ event: this.action, data: this.local_data });
    console.log(this.local_data)
    this.dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      }
    )
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }


 

  ayant_droits: Ayant_droit[] = [
    { value: 'auteur', viewValue: 'auteur' },
    { value: 'compositeur', viewValue: 'compositeur' },
    { value: 'interprète', viewValue: 'interprète' },
    { value: 'arrangeur', viewValue: 'arrangeur' },
    { value: 'editeur', viewValue: 'editeur' },
    { value: 'adptateur', viewValue: 'adptateur' }
  ];


}
