import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAjouterAyantDroitComponent } from './dialog-ajouter-ayant-droit.component';

describe('DialogAjouterAyantDroitComponent', () => {
  let component: DialogAjouterAyantDroitComponent;
  let fixture: ComponentFixture<DialogAjouterAyantDroitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAjouterAyantDroitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAjouterAyantDroitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
