import { Component, OnInit, Input } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

export interface Destination {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'step2-quantite-prix',
  templateUrl: './step2-quantite-prix.component.html',
  styleUrls: ['./step2-quantite-prix.component.scss']
})
export class Step2QuantitePrixComponent implements OnInit {

  @Input() regForm: FormGroup;

  destination_ventes: Destination[] = [
    {value: 'aucun', viewValue: 'Aucun'},
    {value: 'vente public', viewValue: 'Vente directe au public'},
    {value: 'vente distributeur', viewValue: 'Vente via distributeur (Prix de gros HT) '},
  ];

  destination_gratuits: Destination[] = [
    {value: 'aucun', viewValue: 'Aucun'},
    {value: 'promotion', viewValue: 'Promotion du phonogramme vendu (Gratuit)'},
    {value: 'interdit vente', viewValue: 'Hors Commerce -Interdit à la vente (Gratuit)'},
    {value: 'premium', viewValue: 'Premium (Gratuit)'},
  ];

  constructor() { }

  ngOnInit() {
  }


  json(){
    
    let data = this.regForm.value;
    console.log(data);
  }


}
