import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Step2QuantitePrixComponent } from './step2-quantite-prix.component';

describe('Step2QuantitePrixComponent', () => {
  let component: Step2QuantitePrixComponent;
  let fixture: ComponentFixture<Step2QuantitePrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Step2QuantitePrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Step2QuantitePrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
