import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CountriesService } from '../../../countries.service';

export interface Typevoie {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'fabricant',
  templateUrl: './fabricant.component.html',
  styleUrls: ['./fabricant.component.css']
})
export class FabricantComponent implements OnInit {

  @Input() regForm: FormGroup;


  typevoies: Typevoie[] = [
    { value: '', viewValue: '' },
    { value: 'rue', viewValue: 'rue' },
    { value: 'boulevard', viewValue: 'boulevard' },
    { value: 'avenue', viewValue: 'avenue' },
    { value: 'impasse', viewValue: 'impasse' },
    { value: 'chemin', viewValue: 'chemin' },
    { value: 'place', viewValue: 'place' },
    { value: 'square', viewValue: 'square' },
    { value: 'quai', viewValue: 'quai' },
    { value: 'route', viewValue: 'route' },
  ];


  countries: any[]=[];

  constructor(private countriesService: CountriesService) { }

  ngOnInit() {
    this.countriesService.getJSON().subscribe(data => {
      this.countries = data.Countries;
      // console.log(data);
    });
  }

  get f() { 
    return this.regForm.get('intervenantFG').get('fabricantFG')['controls']
 }

  json() {
    let data = this.regForm.value;
    console.log(data);
    console.log(this.regForm.get('intervenantFG').get('fabricantsFG')['controls']);
  }

}
