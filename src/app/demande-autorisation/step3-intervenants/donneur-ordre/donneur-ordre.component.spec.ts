import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonneurOrdreComponent } from './donneur-ordre.component';

describe('DonneurOrdreComponent', () => {
  let component: DonneurOrdreComponent;
  let fixture: ComponentFixture<DonneurOrdreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonneurOrdreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonneurOrdreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
