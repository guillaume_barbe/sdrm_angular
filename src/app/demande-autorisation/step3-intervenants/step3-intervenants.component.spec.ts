import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Step3IntervenantsComponent } from './step3-intervenants.component';

describe('Step3IntervantsComponent', () => {
  let component: Step3IntervenantsComponent;
  let fixture: ComponentFixture<Step3IntervenantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Step3IntervenantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Step3IntervenantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
