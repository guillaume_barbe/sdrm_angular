import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CountriesService } from '../../countries.service';

export interface Typevoie {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'step3-intervenants',
  templateUrl: './step3-intervenants.component.html',
  styleUrls: ['./step3-intervenants.component.css']
})
export class Step3IntervenantsComponent implements OnInit {

  @Input() regForm: FormGroup;


  typevoies: Typevoie[] = [
    { value: '', viewValue: '' },
    { value: 'rue', viewValue: 'rue' },
    { value: 'boulevard', viewValue: 'boulevard' },
    { value: 'avenue', viewValue: 'avenue' },
    { value: 'impasse', viewValue: 'impasse' },
    { value: 'chemin', viewValue: 'chemin' },
    { value: 'place', viewValue: 'place' },
    { value: 'square', viewValue: 'square' },
    { value: 'quai', viewValue: 'quai' },
    { value: 'route', viewValue: 'route' },
  ];


  countries: any[] = [];

  constructor(private countriesService: CountriesService) { }

  ngOnInit() {
    this.countriesService.getJSON().subscribe(data => {
      this.countries = data;
      // console.log(data);
    });
  }


  json() {
    let data = this.regForm.value;
    console.log(data);
  }

}
