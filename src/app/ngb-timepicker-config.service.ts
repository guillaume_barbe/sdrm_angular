import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NgbTimepickerConfigService {
  meridian = false;
  spinners = true;
  seconds = true;
  hourStep = 1;
  minuteStep = 10;
  secondStep = 1;
  disabled = false;
  readonlyInputs = false;
  size: 'small' | 'medium' | 'large' = 'medium';
}
