import { Component, ViewChild,Output} from '@angular/core';
import { ToggleService } from '../toggle.service';
import {MatToolbar} from '@angular/material/toolbar';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {


  @ViewChild('sidenav2', {static: false}) sidenav: MatToolbar;


  constructor(private toggleService: ToggleService) {
  }

  public toggleSidenav() {
    this.toggleService
      .toggle();
 
  
}
}
